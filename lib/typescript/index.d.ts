declare type DemoLibraryType = {
    multiply(a: number, b: number): Promise<number>;
};
declare const _default: DemoLibraryType;
export default _default;
